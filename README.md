# README #
Made by Trond.hanssen.me

### What is this pytonh script for? ###

folders2xml is a simple python script that traverse current folder an all its subfolderes an makes an xml file listing all folders and containing images.
It parse the foldernames and makes tags/keyword describing the containing images based on the foldernames.

It include support for a stoppwordlist with common words.
e.g A image in the folder called "Lisa_and_mike_by_the_Lake" gets taged with:" Lisa, Mike, Lake " but not with "and,by,the" witch are words in the stopwordlist



*Version
 0.1 - BETA

### Requirements ###
- Python version 2.7 or higher.
- The srcipt uses python os and os.walk module witch is OS indepent path traverse.
- The script is tested on Ubuntu and MacOs, but not windows

### How do I get set up? ###

1. Checkout/download the script and place it wherever you want on your machine.
1. Go to the folderyou want to make a xml list of images.
1. Execute the script by using: "python path/to/the/script/imagefolders2xml.py"

The script will then loop thru the current folder and all its subfolders an make a xml file of all folders and images.

The script itself can be placed whereever you want on local machine.

The script start lopping recursivly from where you execute the script.


### Contribution guidelines ###

If you find any bugs or improvment suggestions please use the issue tracker.

If you want to contribute just poke me

### Who do I talk to? ###

Repo owner: Trond Hanssen

### Licens / Warranty ###

===
*WARNING:* Use at your own risk! This script comes with no warranties, guarantees, or
promises of any kind. ===